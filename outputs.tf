# module ecr outputs

output "repository" {
  value = aws_ecr_repository.repository
}

output "lifecycle_policy" {
  value = aws_ecr_lifecycle_policy.policy
}
