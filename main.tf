# module ecr /main

resource "aws_ecr_repository" "repository" {
  for_each             = var.repositories
  name                 = each.value.name
  image_tag_mutability = each.value.image_tag_mutability

  image_scanning_configuration {
    scan_on_push = each.value.scan_on_push
  }
  tags = {
    Environment = var.environment
    ManagedBy   = var.managed_by
  }
}
resource "aws_ecr_lifecycle_policy" "policy" {
  for_each = var.repositories
  repository = aws_ecr_repository.repository[each.key].name

  policy = each.value.lifecycle_policy
}
